#ifndef SMOOTH_H	
#define SMOOTH_H
#include <iostream>
#include "Filtro.hpp"

using namespace std;

class Smooth : public Filtro {
	private: 
		int div;
		int size;
	public:
		Smooth();
		int getDiv();
		int getSize();
		void setDiv(int div);
		void setSize(int size);
		void AplicarFiltro(Imagem &imagem, int div, int size);
};

#endif
