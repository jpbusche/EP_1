#ifndef SHARPEN_H
#define SHARPEN_H
#include <iostream>
#include "Filtro.hpp"

using namespace std;

class Sharpen : public Filtro {

	private:
		int div;
		int size;

	public:
		Sharpen();
		int getDiv();
		int getSize();
		void setDiv(int div);
		void setSize(int size);
		void AplicarFiltro(Imagem &imagem, int div, int size);
};

#endif
