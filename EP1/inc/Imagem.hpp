#ifndef IMAGEM_H
#define IMAGEM_H
#include <iostream>

using namespace std;

class Imagem {
	private: //Atributos Privados
		int Largura;
		int Altura;
		int ValorCinza;
		int *imagem_pgm;
	public: //Metodos Publicos
		Imagem();//Construtor regulador de variaveis
		Imagem(int Largura, int Altura, int ValorCinza);//Construtor de sobrecarga
		//Acessadores de funçao 
		int getLargura();
		int getAltura();
		int getValorCinza();
		
		~Imagem();//Destruto
		
		void mudarPixel(int i, int j, int ValorNovo);
		int getPixel(int i, int j);
		
		//Metodos de manipulaçao de imagem
		void LerImagem(const char fname[]);
		void GravarImagem(const char filename[]);
};
			
#endif
