#ifndef NEGATIVO_H
#define NEGATIVO_H
#include <iostream>
#include "Filtro.hpp"

using namespace std;

class Negativo : public Filtro {
	public:
		Negativo();
		void AplicarFiltro(Imagem &imagem);
};

#endif
