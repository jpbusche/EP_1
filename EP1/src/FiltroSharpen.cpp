#include <iostream>
#include "../inc/FiltroSharpen.hpp"

using namespace std;

Sharpen::Sharpen() {}

void Sharpen::setDiv(int div) {
	this->div = div;
}
void Sharpen::setSize(int size) {
	this->size = size;
}
int Sharpen::getDiv() {
	return div;
}
int Sharpen::getSize() {
	return size;
}
void Sharpen::AplicarFiltro(Imagem &imagem, int div, int size) {
	int i, j, valor, x, y;
	int filtro[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};
	int *auxiliar = new int[imagem.getLargura()*imagem.getAltura()];
	for(i = size/2; i<imagem.getLargura()-size/2; i++) {
		for(j = size/2; j<imagem.getAltura()-size/2; j++) {
			valor = 0;
			for(x=-1; x<=1; x++) {
				for(y=-1; y<=1; y++) {
					valor += filtro[(x+1) + size*(y+1)]*imagem.getPixel((i+x), (y+j));
				}
			}
			valor /= div;
			valor = valor < 0 ? 0 : valor;
			valor = valor > 255 ? 255 : valor;
			auxiliar[i + imagem.getAltura()*j] = valor;
		}
	}
	for(i = 0; i<imagem.getLargura(); i++) {
		for(j = 0; j<imagem.getAltura(); j++) {
			imagem.mudarPixel(i, j, auxiliar[i + imagem.getAltura()*j]);
		}	
	}
	cout << "Filtro sharpen aplicado com sucesso!" << endl;
}
