#include <iostream>
#include "../inc/FiltroNegativo.hpp"

using namespace std;

Negativo::Negativo() {}
void Negativo::AplicarFiltro(Imagem &imagem) {
	int i, j;
	for(i = 0; i<imagem.getLargura(); i++) {
		for(j = 0; j<imagem.getAltura(); j++) {
			int ValorNovo = 255 - imagem.getPixel(i, j);
			imagem.mudarPixel(i, j, ValorNovo);
		}
	}
	cout << "Filtro negativo aplicado com sucesso!" << endl;
}
