#include "../inc/Imagem.hpp"
#include "../inc/Filtro.hpp"
#include "../inc/FiltroNegativo.hpp"
#include "../inc/FiltroSmooth.hpp"
#include "../inc/FiltroSharpen.hpp"

using namespace std;

int main(void) {
	Imagem Lena_Copia(512, 512, 255);
	Lena_Copia.LerImagem("../doc/lena.pgm");
	Lena_Copia.GravarImagem("../bin/CopiaLena.pgm");
	
	Imagem Lena_Negativo(512, 512, 255);
	Lena_Negativo.LerImagem("../doc/lena.pgm");
	Negativo filtro_Negativo;
	filtro_Negativo.AplicarFiltro(Lena_Negativo);
	Lena_Negativo.GravarImagem("../bin/NegativoLena.pgm");
	
	Imagem Lena_Smooth(512, 512, 255);
	Lena_Smooth.LerImagem("../doc/lena.pgm");
	Smooth filtroSmooth;
	filtroSmooth.setDiv(9);
	filtroSmooth.setSize(3);
	filtroSmooth.AplicarFiltro(Lena_Smooth, filtroSmooth.getDiv(), filtroSmooth.getSize());
	Lena_Smooth.GravarImagem("../bin/SmoothLena.pgm");
	
	Imagem Lena_Sharpen(512, 512, 255);
	Lena_Sharpen.LerImagem("../doc/lena.pgm");
	Sharpen filtro_Sharpen;
	filtro_Sharpen.setDiv(1);
	filtro_Sharpen.setSize(3);
	filtro_Sharpen.AplicarFiltro(Lena_Sharpen, filtro_Sharpen.getDiv(), filtro_Sharpen.getSize());
	Lena_Sharpen.GravarImagem("../bin/SharpenLena.pgm");

	return 0;
}
