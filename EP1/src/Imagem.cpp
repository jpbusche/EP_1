#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <stdlib.h> 
#include "../inc/Imagem.hpp"

using namespace std;

Imagem::Imagem() {
	Largura = 0;
	Altura = 0;
	ValorCinza = 0;
	imagem_pgm = NULL;
}
Imagem::Imagem(int Largura, int Altura, int ValorCinza) {
	this->Largura = Largura;
	this->Altura = Altura;
	this->ValorCinza = ValorCinza;
	imagem_pgm = new int[Largura*Altura];
}
int Imagem::getLargura() {
	return Largura;
}
int Imagem::getAltura() {
	return Altura;
}
int Imagem::getValorCinza() {
	return ValorCinza;
}
Imagem::~Imagem() {
	delete []imagem_pgm;
}
void Imagem::LerImagem(const char fname[]) {
	int i, j;
	unsigned char *charImagem;
	char header[100], *pointer;
	ifstream ifp;
	ifp.open(fname, ios::in | ios::binary);
	if(!ifp) {
		cout << "Nao e possivel ler a imagem: " << fname << endl;
		exit(1);
	}
	ifp.getline(header, 100, '\n');
	if((header[0]!=80) || (header[1]!=53)) {
		cout << "A imagem " << fname << "nao eh PGM" << endl;
		exit(1);
	}
	ifp.getline(header, 100, '\n');
	while(header[0]=='#') 
		ifp.getline(header, 100, '\n');
		Largura = strtol(header, &pointer, 0);
		Altura = atoi(pointer);
		ifp.getline(header, 100, '\n');
		ValorCinza = strtol(header, &pointer, 0);
		charImagem = (unsigned char *) new unsigned char[Largura*Altura];
		ifp.read( reinterpret_cast<char *>(charImagem), (Largura*Altura)*sizeof(unsigned char));
		if(ifp.fail()) {
			cout << "A imagem" << fname << "esta com o tamanho errado" << endl;
			exit(1);
		}
		ifp.close();
	int valor;
	for(i=0; i<Largura; i++) {
		for(j=0; j<Altura; j++) {
			valor = (int)charImagem[i*Altura+j];
			imagem_pgm[i*Altura+j] = valor;
		}
	}
	delete []charImagem;
}	
void Imagem::GravarImagem(const char filename[]) {
	int i, j;
	unsigned char *charImage;
	ofstream outfile(filename);
	charImage = (unsigned char *) new unsigned char [Largura*Altura];
	int valor;
    	for(i=0; i<Largura; i++) {
        	for(j=0; j<Altura; j++) {
			valor = imagem_pgm[i*Altura+j];
			charImage[i*Altura+j] = (unsigned char)valor;
        	}
    	}
	if (!outfile.is_open()){
		cout << "Nao e possivel abrir o arquivo de saida"  << filename << endl;
		exit(1);
	}
	outfile << "P5" << endl;
    	outfile << Largura << " " << Altura << endl;
    	outfile << 255 << endl;
	outfile.write(reinterpret_cast<char *>(charImage), (Largura*Altura)*sizeof(unsigned char));	
	cout << "Arquivo " << filename << " criado com sucesso!" << endl;
	outfile.close();
}
void Imagem::mudarPixel(int i, int j, int ValorNovo) {
    	imagem_pgm[i*Altura+j] = ValorNovo;
}
int Imagem::getPixel(int i, int j){
    	return imagem_pgm[i*Altura+j];
}
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
